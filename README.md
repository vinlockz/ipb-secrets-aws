# IPB Config Generator for AWS Secrets Manager

## Installation
```
// With NPM
npm install -g ipbgen

// With Yarn
yarn global add ipbgen
```

## Usage
```
ipbgen -s <Secret Name> -r <Forums Root File Path>

ipbgen -s prod/secret -r /var/www/forums
```

### Required Secrets
**SQL_HOST** - MySQL Database Host  
**SQL_DATABASE** - MySQL Database Name  
**SQL_USER** - MySQL Database Username  
**SQL_PASS** - MySQL Database Password  
**BASE_URL** - Base Forum URL  
**BOARD_START** - UNIX Timestamp for when the IPB was installed  
  
### Optional Secrets  
**SQL_PORT** - MySQL Database Port - Default: `3306`  
**SQL_SOCKET** - MySQL Database Socket - Default: `null`  
**SQL_TABLE_PREFIX** - MySQL Database Table Prefix - Default: `null`    
**SQL_UTF8MB4** - MySQL Database UTF8MB4 Setting - Default: `true`  
**INSTALLED** - Forum is installed - Default: `true`  
**GUEST_GROUP** - Guest Group ID - Default: `2`  
**MEMBER_GROUP** - Member Group ID - Default: `3`  
**ADMIN_GROUP** - Admin Group ID - Default: `4`  

### How to use `constants.php` with Secrets
Any secret prefixed with `CONSTANTS_` will be used in the `constants.php` file after removing the `CONSTANTS_` prefix.

Example:  
```
CONSTANTS_IN_DEV = true

// becomes

define('IN_DEV', true);
```

### Helpful Links
**Using `constants.php`**  
https://invisioncommunity.com/4guides/advanced-options/configuration-options/using-constantsphp-r25/


