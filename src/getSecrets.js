const AWS = require('aws-sdk');


module.exports = async (secretPath, region = 'us-west-2') => {
  const secretsmanager = new AWS.SecretsManager({
    apiVersion: '2017-10-17',
    region,
  });

  const params = {
    SecretId: secretPath
  };
  const secret = await secretsmanager.getSecretValue(params).promise();
  return JSON.parse(secret.SecretString);
};