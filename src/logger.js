const winston = require('winston');
//
// const format = printf(({ level, message, label, timestamp }) => {
//   return `${timestamp} [${label}] ${level}: ${message}`;
// });

const logger = winston.createLogger({
  level: 'info',
  transports: [
    winston.transports.Console(),
  ],
});

module.exports = logger;
