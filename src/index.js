#!/usr/bin/env node
const commandLineArgs = require('command-line-args');
const args = process.argv;
const getSecret = require('./getSecrets');
const confGlobalGenerator = require('./fileTemplates/confGlobalGenerator');
const constantsGenerator = require('./fileTemplates/constantsGenerator');

const optionsDefinitions = [
  { name: 'secretsPath', alias: 's', type: String, defaultOption: true },
  { name: 'forumsRootPath', alias: 'r', type: String },
];
const options = commandLineArgs(optionsDefinitions);

/**
 * Generate Forum Configs
 * @param {string} secretsPath AWS Secrets Manager Path
 * @param {string} forumsRootPath Forums Root Path
 */
const generateConfigs = (secretsPath, forumsRootPath) => {
  getSecret(secretsPath)
    .then((secrets) => {
      confGlobalGenerator(forumsRootPath, {
        sqlHost: secrets.SQL_HOST,
        sqlDatabase: secrets.SQL_DATABASE,
        sqlUser: secrets.SQL_USER,
        sqlPass: secrets.SQL_PASS,
        boardStart: secrets.BOARD_START,
        baseUrl: secrets.BASE_URL,
        sqlPort: secrets.SQL_PORT,
        sqlSocket: secrets.SQL_SOCKET,
        sqlTablePrefix: secrets.SQL_TABLE_PREFIX,
        sqlUtf8mb4: secrets.SQL_UTF8MB4,
        installed: secrets.INSTALLED,
        guestGroup: secrets.GUEST_GROUP,
        memberGroup: secrets.MEMBER_GROUP,
        adminGroup: secrets.ADMIN_GROUP,
      });

      const constants = Object.keys(secrets)
        .filter((secretKey) => secretKey.startsWith('CONSTANTS_'))
        .map((constantKey) => {
          let value = secrets[ constantKey ];
          if (!['false', 'true'].includes(value.toLowerCase())) {
            value = `'${value}'`;
          }
          return {
            key: constantKey.substring(10),
            value,
          };
        });

      constantsGenerator(forumsRootPath, constants);
    });
};

const { secretsPath, forumsRootPath } = options;

if (secretsPath === undefined) {
  throw new Error('--secretsPath or -s must be defined.');
}
if (forumsRootPath === undefined) {
  throw new Error('--forumsRootPath or -r must be defined.');
}

generateConfigs(secretsPath, forumsRootPath);
