const Handlebars = require('handlebars');

module.exports = (template, context) => {
  const compiled = Handlebars.compile(template);
  return compiled(context);
};
