const fs = require('fs');
const compileTemplate = require('../compileTemplate');

const template = `<?php
{{#each constants}}
define('{{ this.key }}', {{{ this.value }}});
{{/each}}
`;

/**
 * Generate constants.php
 * @param {string} path Path to constants.php
 * @param {object} constants Constants
 */
module.exports = (path, constants) => {
  const content = compileTemplate(template, { constants });
  let fullPath = `${path}/constants.php`;
  if (path.endsWith('/')) {
    fullPath = `${path}constants.php`;
  }
  fs.writeFileSync(fullPath, content);
};
