const fs = require('fs');
const yup = require('yup');
const compileTemplate = require('../compileTemplate');

const template = `<?php

$INFO = array(
    'sql_host' => '{{sqlHost}}',
    'sql_database' => '{{sqlDatabase}}',
    'sql_user' => '{{sqlUser}}',
    'sql_pass' => '{{sqlPass}}',
    'sql_port' => {{sqlPort}},
    'sql_socket' => '{{sqlSocket}}',
    'sql_tbl_prefix' => '{{sqlTablePrefix}}',
    'sql_utf8mb4' => {{sqlUtf8mb4}},
    'board_start' => {{boardStart}},
    'installed' => {{installed}},
    'base_url' => '{{baseUrl}}',
    'guest_group' => {{guestGroup}},
    'member_group' => {{memberGroup}},
    'admin_group' => {{adminGroup}},
);
`;

/**
 * Generate conf_global.php
 * @param {string} path Path to conf_global.php
 * @param {object} options Template options
 * @param {string} options.sqlHost SQL Host
 * @param {string} options.sqlDatabase SQL Database Name
 * @param {string} options.sqlUser SQL Username
 * @param {string} options.sqlPass SQL Password
 * @param {string} options.baseUrl Base URL
 * @param {number} options.boardStart Board Start UNIX Timestamp
 * @param {number} [options.sqlPort=3306] SQL Port
 * @param {string} [options.sqlSocket=undefined] SQL Socket
 * @param {string} [options.sqlTablePrefix=undefined] SQL Table Prefix
 * @param {boolean} [options.sqlUtf8mb4=true] SQL UTF8MB4
 * @param {boolean} [options.installed=true] Installed?
 * @param {number} [options.guestGroup=2] Guest Group ID
 * @param {number} [options.memberGroup=3] Member Group ID
 * @param {number} [options.adminGroup=4] Admin Group ID
 */
module.exports = (path, options) => {
  const schema = yup.object().shape({
    sqlHost: yup.string().required(),
    sqlDatabase: yup.string().required(),
    sqlUser: yup.string().required(),
    sqlPass: yup.string().required(),
    baseUrl: yup.string().required(),
    boardStart: yup.number().required(),
    sqlPort: yup.number().default(3306),
    sqlSocket: yup.string().default(''),
    sqlTablePrefix: yup.string().default(''),
    sqlUtf8mb4: yup.boolean().default(true),
    installed: yup.boolean().default(true),
    guestGroup: yup.number().default(2),
    memberGroup: yup.number().default(3),
    adminGroup: yup.number().default(4),
  });

  // Validate
  schema.validateSync(options);

  // Create File
  const context = schema.cast(options);
  const content = compileTemplate(template, context);
  let fullPath = `${path}/conf_global.php`;
  if (path.endsWith('/')) {
    fullPath = `${path}conf_global.php`;
  }
  fs.writeFileSync(fullPath, content);
};
